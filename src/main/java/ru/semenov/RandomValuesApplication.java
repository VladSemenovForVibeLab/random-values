package ru.semenov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomValuesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomValuesApplication.class, args);
	}

}
