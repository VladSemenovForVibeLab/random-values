package ru.semenov.service.impl;

import io.micrometer.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.domain.entity.UserEntity;
import ru.semenov.domain.entity.UserEntityRepository;
import ru.semenov.domain.entity.enums.Role;
import ru.semenov.domain.entity.exception.ResourceNotFoundException;
import ru.semenov.rest.model.in.JWTRequest;
import ru.semenov.rest.model.in.UserModelDTORequestForRegister;
import ru.semenov.rest.model.out.JWTResponse;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.rest.model.out.UserModelResponse;
import ru.semenov.security.JwtTokenProvider;
import ru.semenov.service.IMailSender;
import ru.semenov.service.IUserService;
import ru.semenov.util.UserModelDTOUtil;
import ru.semenov.util.UserModelDTOUtilResponse;

import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements IUserService {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserEntityRepository userModelRepository;
    private final UserModelDTOUtil userModelDTOUtil;
    private final UserModelDTOUtilResponse userModelDTOUtilResponse;
    private final PasswordEncoder passwordEncoder;
    private final IMailSender mailSender;

    public UserServiceImpl(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserEntityRepository userModelRepository, UserModelDTOUtil userModelDTOUtil, UserModelDTOUtilResponse userModelDTOUtilResponse, PasswordEncoder passwordEncoder, IMailSender mailSender) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userModelRepository = userModelRepository;
        this.userModelDTOUtil = userModelDTOUtil;
        this.userModelDTOUtilResponse = userModelDTOUtilResponse;
        this.passwordEncoder = passwordEncoder;
        this.mailSender = mailSender;
    }

    @Override
    @Transactional
    public boolean createUserModel(UserModelDTORequestForRegister userModelDTORequestForRegister) {
        if(userModelRepository.findUserModelByEmail(userModelDTORequestForRegister.getEmail())!= null) {
            log.error("User with email {} already exists", userModelDTORequestForRegister.getEmail());
        }
        toUserModelForRegister(userModelDTORequestForRegister);
        log.info("User with email {} created", userModelDTORequestForRegister.getEmail());
        return true;
    }

    @Override
    public UserEntity getByEmail(String email) {
        return userModelRepository.findUserModelByEmail(email);
    }

    @Override
    public UserModelDTORequestForRegister findById(String userId) {
        return userModelDTOUtil.toDTO(
                userModelRepository.findUserModelByUid(userId)
        );
    }

    @Override
    public JWTResponse login(JWTRequest loginRequest) {
        JWTResponse jwtResponse = new JWTResponse();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),loginRequest.getPassword()));
        UserEntity user = userModelRepository.findUserModelByEmail(loginRequest.getEmail());
        jwtResponse.setId(user.getUid());
        jwtResponse.setEmail(user.getEmail());
        jwtResponse.setName(user.getName());
        jwtResponse.setAccessToken(jwtTokenProvider.createAccessToken(user.getUid(),user.getEmail(),user.getRoles()));
        return jwtResponse;
    }

    @Override
    public UserModelResponse userModelProfile(String id) {
        return userModelDTOUtilResponse.toDTO(
                userModelRepository.findUserModelByUid(id)
        );
    }

    @Override
    @Transactional
    public MessageResponse adminProfile(String id) {
        UserEntity userModel = userModelRepository.findUserModelByUid(id);
        userModel.getRoles().add(Role.ROLE_ADMIN);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"ADMIN");
    }

    @Override
    @Transactional
    public MessageResponse adminEnabled(String id) {
        UserEntity userModel = userModelRepository.findUserModelByUid(id);
        userModel.setActive(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"ACTIVE -> FALSE");
    }

    @Override
    @Transactional
    public MessageResponse adminCredentialsExpired(String uid) {
        UserEntity userModel = userModelRepository.findUserModelByUid(uid);
        userModel.setExpired(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"CREDENTIALS EXPIRED -> FALSE");
    }

    @Override
    @Transactional
    public MessageResponse adminAccountLocked(String uid) {
        UserEntity userModel = userModelRepository.findUserModelByUid(uid);
        userModel.setBlock(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"CREDENTIALS EXPIRED -> FALSE");
    }

    @Override
    @Transactional
    public MessageResponse adminAccountExpired(String uid) {
        UserEntity userModel = userModelRepository.findUserModelByUid(uid);
        userModel.setAccountExpired(false);
        userModelRepository.save(userModel);
        return new MessageResponse(HttpStatus.OK,"ACCOUNT EXPIRED -> FALSE");
    }

    @Override
    public boolean activateUserModel(String code) {
        UserEntity userModel = userModelRepository.findUserModelByActiveCode(code)
                .orElseThrow(
                        () -> new ResourceNotFoundException("User model not found!"));
        userModel.setActive(true);
        userModel.setActiveCode(null);
        userModelRepository.save(userModel);
        return true;
    }


    public void toUserModelForRegister(UserModelDTORequestForRegister userModelDTORequestForRegister) {
        UserEntity userModel = new UserEntity();
        userModel.setEmail(userModelDTORequestForRegister.getEmail());
        userModel.setPassword(passwordEncoder.encode(userModelDTORequestForRegister.getPassword()));
        userModel.setPhoneNumber(userModelDTORequestForRegister.getPhoneNumber());
        userModel.setName(userModelDTORequestForRegister.getName());
        userModel.getRoles().add(Role.ROLE_USER);
        userModel.setActive(false);
        userModel.setBlock(true);
        userModel.setExpired(true);
        userModel.setAccountExpired(true);
        userModel.setActiveCode(UUID.randomUUID().toString());
        userModelRepository.save(userModel);
        if(!StringUtils.isEmpty(userModel.getEmail())){
            String message = String.format(
                    "Привет, %s! \n"+
                            "Добро пожаловать на новый сайт!!  \n"+
                            "Пожалуйста, чтобы продолжить использование данного сайта вам нужно перейти по ссылке и подтвердить, что это вы: http://localhost:8080/api/v1/rest/users/activate/%s",
                    userModel.getName(),
                    userModel.getActiveCode()
            );
            mailSender.sendMail(userModel.getEmail(),"Activation code",message);
        }
    }
}

