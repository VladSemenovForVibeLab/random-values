package ru.semenov.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.semenov.domain.entity.CategoryEntity;
import ru.semenov.domain.entity.CategoryEntityRepository;
import ru.semenov.domain.entity.ValueEntity;
import ru.semenov.domain.entity.ValueEntityRepository;
import ru.semenov.rest.model.in.CreateCategoryRestIn;
import ru.semenov.rest.model.in.UpdateCategoryRestIn;
import ru.semenov.rest.model.out.CategoryRestOut;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.rest.model.out.ValueRestOut;
import ru.semenov.service.RandomValuesService;
import ru.semenov.rest.model.out.CategoryOut;
import ru.semenov.util.EntityDTOUtil;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class RandomValuesServiceImpl implements RandomValuesService {
    private final CategoryEntityRepository categoryEntityRepository;
    private final ValueEntityRepository valueEntityRepository;
    private final EntityDTOUtil<CategoryOut, CategoryRestOut, CategoryEntity,CreateCategoryRestIn,UpdateCategoryRestIn> entityDTOUtil;

    public RandomValuesServiceImpl(CategoryEntityRepository categoryEntityRepository, ValueEntityRepository valueEntityRepository, EntityDTOUtil<CategoryOut, CategoryRestOut, CategoryEntity,CreateCategoryRestIn,UpdateCategoryRestIn> entityDTOUtil) {
        this.categoryEntityRepository = categoryEntityRepository;
        this.valueEntityRepository = valueEntityRepository;
        this.entityDTOUtil = entityDTOUtil;
    }

    @Override
    public List<CategoryOut> getAllCategories() {
        return entityDTOUtil.convertEntityToServiceList(
                categoryEntityRepository.findAll());
    }

    @Override
    public CategoryOut addNewCategory(CreateCategoryRestIn createCategoryRestIn) {
        return entityDTOUtil.convertEntityToService(categoryEntityRepository.save(entityDTOUtil.toEntityForRequest(createCategoryRestIn)));
    }

    @Override
    public CategoryRestOut updateCategory(UpdateCategoryRestIn updateCategoryRestIn, String categoryId) {
        return entityDTOUtil.convertCategoryRestOut(
                categoryEntityRepository.save(
                        entityDTOUtil.convertUpdateRestInToEntity(updateCategoryRestIn)));
    }

    @Override
    public MessageResponse deleteCategory(String categoryId) {
        categoryEntityRepository.deleteById(categoryId);
        return new MessageResponse(HttpStatus.NO_CONTENT,"Модель успешно удалена!!", LocalDateTime.now());
    }

    @Override
    public ValueRestOut getValueByCategoryId(String categoryId) {
        return null;
    }
}
