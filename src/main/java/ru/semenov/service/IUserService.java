package ru.semenov.service;

import ru.semenov.domain.entity.UserEntity;
import ru.semenov.rest.model.in.JWTRequest;
import ru.semenov.rest.model.in.UserModelDTORequestForRegister;
import ru.semenov.rest.model.out.JWTResponse;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.rest.model.out.UserModelResponse;

public interface IUserService {
    boolean createUserModel(UserModelDTORequestForRegister userModelDTORequestForRegister);

    UserEntity getByEmail(String email);

    UserModelDTORequestForRegister findById(String userId);
    JWTResponse login(JWTRequest loginRequest);

    UserModelResponse userModelProfile(String id);

    MessageResponse adminProfile(String id);

    MessageResponse adminEnabled(String id);

    MessageResponse adminCredentialsExpired(String uid);

    MessageResponse adminAccountLocked(String uid);

    MessageResponse adminAccountExpired(String uid);


    boolean activateUserModel(String code);

}
