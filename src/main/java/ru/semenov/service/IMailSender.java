package ru.semenov.service;

public interface IMailSender {
    void sendMail(String to, String subject, String text);
}

