package ru.semenov.service;

import ru.semenov.rest.model.in.CreateCategoryRestIn;
import ru.semenov.rest.model.in.UpdateCategoryRestIn;
import ru.semenov.rest.model.out.CategoryOut;
import ru.semenov.rest.model.out.CategoryRestOut;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.rest.model.out.ValueRestOut;

import java.util.List;

public interface RandomValuesService {
    List<CategoryOut> getAllCategories();

    CategoryOut addNewCategory(CreateCategoryRestIn createCategoryRestIn);

    CategoryRestOut updateCategory(UpdateCategoryRestIn updateCategoryRestIn, String categoryId);

    MessageResponse deleteCategory(String categoryId);

    ValueRestOut getValueByCategoryId(String categoryId);
}
