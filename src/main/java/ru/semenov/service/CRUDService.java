package ru.semenov.service;

import ru.semenov.rest.model.out.MessageResponse;

import java.util.List;

public interface CRUDService<K,D> {

    D createEntity(D entityForCreate);

    D findById(K walletUuid);

    List<D> findAll();

    D updateEntity(D objectForUpdate);

    MessageResponse deleteEntity(D objectForDelete);
}
