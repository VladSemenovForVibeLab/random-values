package ru.semenov.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.domain.entity.CategoryEntity;
import ru.semenov.rest.model.in.CreateCategoryRestIn;
import ru.semenov.rest.model.in.UpdateCategoryRestIn;
import ru.semenov.rest.model.out.CategoryRestOut;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.service.RandomValuesService;
import ru.semenov.rest.model.out.CategoryOut;
import ru.semenov.util.EntityDTOUtil;

import java.util.List;

@RestController
@RequestMapping(CategoryController.CATEGORY_CONTROLLER_URL)
public class CategoryController {
    private final EntityDTOUtil<CategoryOut,CategoryRestOut, CategoryEntity,CreateCategoryRestIn,UpdateCategoryRestIn> entityDTOUtil;
    private final RandomValuesService randomValuesService;
    public static final String CATEGORY_CONTROLLER_URL ="api/v1/category";

    public CategoryController(EntityDTOUtil entityDTOUtil, RandomValuesService randomValuesService) {
        this.entityDTOUtil = entityDTOUtil;
        this.randomValuesService = randomValuesService;
    }

    @GetMapping
    private ResponseEntity<List<CategoryRestOut>> getAllCategories(){
        return new ResponseEntity<>(entityDTOUtil.convertServiceToControllerList(randomValuesService.getAllCategories()), HttpStatus.OK);
    }

    @PostMapping("/add")
    private ResponseEntity<CategoryOut> addNewCategory(@RequestBody CreateCategoryRestIn createCategoryRestIn){
        return new ResponseEntity<>(randomValuesService.addNewCategory(createCategoryRestIn),HttpStatus.OK);
    }

    @PutMapping("/update/{categoryId}")
    private ResponseEntity<CategoryRestOut> updateCategory(@RequestBody UpdateCategoryRestIn updateCategoryRestIn,
                                                           @PathVariable("categoryId") String categoryId){
            return new ResponseEntity<>(randomValuesService.updateCategory(updateCategoryRestIn,categoryId),HttpStatus.OK);
    }
    @DeleteMapping("/delete/{categoryId}")
    private ResponseEntity<MessageResponse> deleteCategory(@PathVariable("categoryId") String categoryId){
        return new ResponseEntity<>(randomValuesService.deleteCategory(categoryId),HttpStatus.NO_CONTENT);
    }

}
