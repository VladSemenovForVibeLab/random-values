package ru.semenov.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.semenov.rest.model.in.JWTRequest;
import ru.semenov.rest.model.in.UserModelDTORequestForRegister;
import ru.semenov.rest.model.out.JWTResponse;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.rest.model.out.UserModelResponse;
import ru.semenov.service.IUserService;

import java.time.LocalDateTime;

@RestController
@RequestMapping(UserModelRestController.USER_MODEL_URL)
public class UserModelRestController {
    private final IUserService iUserService;
    public static final String USER_MODEL_URL = "/api/v1/rest/users";

    public UserModelRestController(IUserService iUserService) {
        this.iUserService = iUserService;
    }

    @PostMapping("/register")
    public ResponseEntity<MessageResponse> register(@RequestBody UserModelDTORequestForRegister userModelDTORequestForRegister) {
        iUserService.createUserModel(userModelDTORequestForRegister);
        return new ResponseEntity<>(new MessageResponse(HttpStatus.CREATED,"Пользователь успешно зарегестрирован!" , LocalDateTime.now()),HttpStatus.OK);
    }
    @PostMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return iUserService.login(loginRequest);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserModelResponse> userModelProfile(@PathVariable String id){
        return new ResponseEntity<>(iUserService.userModelProfile(id),HttpStatus.OK);
    }
    @GetMapping("/activate/{code}")
    public ResponseEntity<MessageResponse> activate(@PathVariable String code){
        boolean isActivated = iUserService.activateUserModel(code);
        if(isActivated) {
            return new ResponseEntity<>(new MessageResponse(HttpStatus.OK,"Отлично, пользователь успешно активирован!!",LocalDateTime.now()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new MessageResponse(HttpStatus.BAD_REQUEST,"User not activated"),HttpStatus.BAD_REQUEST);
    }

}

