package ru.semenov.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.rest.model.out.MessageResponse;
import ru.semenov.service.CRUDService;

import java.util.List;

public abstract class CRUDRestController<K,D> {
    abstract CRUDService<K,D> getService();
    @PostMapping
    public ResponseEntity<D> createEntity(@RequestBody D entityForCreate){
        return ResponseEntity.ok(getService().createEntity(entityForCreate));
    }
    @GetMapping("/{id}")
    public ResponseEntity<D> findById(@PathVariable K id){
        D object = getService().findById(id);
        if(object==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(object);
    }
    @GetMapping("/all")
    public ResponseEntity<List<D>> findAll(){
        List<D> objects = getService().findAll();
        return ResponseEntity.ok(objects);
    }
    @PutMapping("/update")
    public ResponseEntity<D> updateEntity(@RequestBody D objectForUpdate){
        D updatedObject = getService().updateEntity(objectForUpdate);
        return ResponseEntity.ok(updatedObject);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<MessageResponse> deleteEntity(@PathVariable K id){
        D objectForDelete = getService().findById(id);
        return ResponseEntity.ok(getService().deleteEntity(objectForDelete));
    }

}
