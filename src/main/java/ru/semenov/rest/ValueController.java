package ru.semenov.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.rest.model.out.ValueRestOut;
import ru.semenov.service.RandomValuesService;

@RestController
@RequestMapping(ValueController.VALUE_CONTROLLER_URL)
public class ValueController {
    public static final String VALUE_CONTROLLER_URL="api/v1/value";

    private final RandomValuesService randomValuesService;

    public ValueController(RandomValuesService randomValuesService) {
        this.randomValuesService = randomValuesService;
    }

    @GetMapping("/{categoryId")
    public ResponseEntity<ValueRestOut> getValueByCategoryId(String categoryId) {
        return new ResponseEntity<>(randomValuesService.getValueByCategoryId(categoryId), HttpStatus.OK);
    }
}
