package ru.semenov.rest.model.in;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Schema(name = "Create category")
public class CreateCategoryRestIn extends BaseRestIn{
    @Schema(description = "Наименование категории",example = "Кружки")
    private String name;
    @Schema(description = "Описание категории",example = "Отличные кружкы!!")
    private String description;
}
