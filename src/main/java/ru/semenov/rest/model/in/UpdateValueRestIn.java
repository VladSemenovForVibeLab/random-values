package ru.semenov.rest.model.in;

import lombok.*;
import org.hibernate.query.sql.internal.ParameterRecognizerImpl;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UpdateValueRestIn extends BaseRestIn{
    private String name;
    private String description;
}
