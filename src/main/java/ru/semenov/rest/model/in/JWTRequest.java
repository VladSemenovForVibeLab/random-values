package ru.semenov.rest.model.in;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Request for login")
public class JWTRequest {
    @Schema(description = "email",example = "ooovladislavchik@gmail.com")
    private String email;
    @Schema(description = "password for request",example = "1q2w3e")
    private String password;
}

