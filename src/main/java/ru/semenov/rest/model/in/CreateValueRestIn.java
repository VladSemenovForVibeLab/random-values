package ru.semenov.rest.model.in;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode

public class CreateValueRestIn extends BaseRestIn{
    private String name;
    private String description;
}
