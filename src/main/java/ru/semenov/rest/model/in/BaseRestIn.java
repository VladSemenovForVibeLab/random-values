package ru.semenov.rest.model.in;

import lombok.*;

public abstract class BaseRestIn {
    private String name;
    private String description;
}
