package ru.semenov.rest.model.out;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MessageResponse {
    @Schema(description = "status for message response",example = "404")
    private HttpStatus status;
    @Schema(description = "message for response",example = "This email is not registered")
    private String message;
    private Object data;
    public MessageResponse(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

}

