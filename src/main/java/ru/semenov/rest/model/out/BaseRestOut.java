package ru.semenov.rest.model.out;

import lombok.Builder;
import lombok.Getter;


public abstract class BaseRestOut {
    private String id;
    private String name;
    private String description;
}
