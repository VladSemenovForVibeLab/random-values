package ru.semenov.rest.model.out;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.domain.entity.UserEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserModelResponse {
    @Schema(description = "email", example = "ooovladislavchik@gmail.com")
    private String email;
    @Schema(description = "phone for request", example = "89184444444")
    private String phoneNumber;
    @Schema(description = "name for request", example = "Vlad")
    private String name;
    public UserModelResponse(UserEntity userModel){
        this.email = userModel.getEmail();
        this.phoneNumber = userModel.getPhoneNumber();
        this.name = userModel.getName();
    }
}

