package ru.semenov.rest.model.out;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class ValueRestOut extends BaseRestOut {
    private String id;
    private String name;
    private String description;
}
