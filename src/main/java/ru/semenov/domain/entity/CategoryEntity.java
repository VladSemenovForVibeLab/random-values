package ru.semenov.domain.entity;

import jakarta.persistence.*;
import lombok.*;
import org.apache.catalina.User;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Table(name = CategoryEntity.TABLE_NAME)
public class CategoryEntity implements Serializable,Comparable<CategoryEntity> {
    public static final String TABLE_NAME = "category_entity";
    public static final String CATEGORY_ID_COLUMN_NAME = "id";
    private static final String CATEGORY_NAME_COLUMN_NAME = "name";
    private static final String CATEGORY_DESCRIPTION_COLUMN_NAME = "description";
    private static final String CATEGORY_ACTIVE_COLUMN_NAME="active";
    private static final String CATEGORY_CREATE_COLUMN_NAME="createAt";
    private static final String CATEGORY_UPDATE_COLUMN_NAME="updateAt";
    //    @jakarta.persistence.Id
    //    private UUID uuid;
    @jakarta.persistence.Id
    @Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = CATEGORY_ID_COLUMN_NAME, unique = true, nullable = false)
    private String categoryId;
    @Column(name = CATEGORY_NAME_COLUMN_NAME, nullable = false, unique = true)
    private String name;
    @Column(name = CATEGORY_DESCRIPTION_COLUMN_NAME)
    private String description;
    @Column(name = CATEGORY_ACTIVE_COLUMN_NAME)
    private boolean active;
    @Column(name = CATEGORY_CREATE_COLUMN_NAME)
    private LocalDateTime createdAt;
    @Column(name = CATEGORY_UPDATE_COLUMN_NAME)
    private LocalDateTime updatedAt;
    @OneToMany(mappedBy = "categoryEntity")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<ValueEntity> values;
    @ManyToMany
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JoinTable(
            name = "category_entities_user_entities",
            joinColumns = @JoinColumn(name = "category_entity_id"),
            inverseJoinColumns = @JoinColumn(name = "user_entity_id")
    )
    private List<UserEntity> userEntities;
    @PrePersist
    public void onCreate(){
        createdAt = LocalDateTime.now();
        updatedAt= LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        updatedAt = LocalDateTime.now();
    }
    @Transient
    private final Object lock = new Object();
    public void lock(){
        synchronized (lock) {

        }
    }

    @Override
    public int compareTo(CategoryEntity o) {
        return name.compareTo(o.getName());
    }
}
