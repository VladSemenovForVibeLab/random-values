package ru.semenov.domain.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Entity
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
@Getter
@Setter
@Table(name = ValueEntity.TABLE_NAME)
public class ValueEntity implements Serializable,Comparable<ValueEntity>{
    public static final String TABLE_NAME = "value_entity";
    private static final String VALUE_ID_COLUMN_NAME="id";
    private static final String VALUE_CREATE_COLUMN_NAME="createAt";
    private static final String VALUE_UPDATE_COLUMN_NAME="updateAt";
    private static final String VALUE_NAME_COLUMN_NAME="name";
    private static final String VALUE_DESCRIPTION_COLUMN_NAME="description";
    private static final String VALUE_CATEGORY_COLUMN_NAME="category";
    private static final String VALUE_ACTIVE_COLUMN_NAME="active";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = VALUE_ID_COLUMN_NAME,unique = true, nullable = false)
    private String valueId;
    @Column(name = VALUE_NAME_COLUMN_NAME,nullable = false)
    private String name;
    @Column(name = VALUE_DESCRIPTION_COLUMN_NAME)
    private String description;
    @Column(name = VALUE_CREATE_COLUMN_NAME,nullable =false)
    private LocalDateTime createAt;
    @Column(name = VALUE_ACTIVE_COLUMN_NAME)
    private boolean isActive;
    @Column(name = VALUE_UPDATE_COLUMN_NAME,nullable =false)
    private LocalDateTime updateAt;
    @JoinColumn(name = "category_entity_id",referencedColumnName = "id")
    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private CategoryEntity categoryEntity;
    @ManyToMany
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinTable(
            name = "value_entities_user_entities",
            joinColumns = @JoinColumn(name = "value_entity_id"),
            inverseJoinColumns = @JoinColumn(name = "user_entity_uid")
    )
    private List<UserEntity> userEntities;
    @PrePersist
    public void onCreate(){
        createAt=LocalDateTime.now();
        updateAt=LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        updateAt=LocalDateTime.now();
    }
    @Override
    public int compareTo(ValueEntity o) {
        return this.description.compareTo(o.description);
    }
}
