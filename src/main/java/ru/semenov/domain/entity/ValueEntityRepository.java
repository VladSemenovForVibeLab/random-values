package ru.semenov.domain.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ValueEntityRepository extends JpaRepository<ValueEntity, String> {
}