package ru.semenov.domain.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, String> {
    UserEntity findUserModelByEmail(String email);

    UserEntity findUserModelByUid(String userId);

    Optional<UserEntity> findUserModelByActiveCode(String code);
}