package ru.semenov.domain.entity.exception;

public class ResourceMappingException extends RuntimeException {
    public ResourceMappingException(String message) {
        super(message);
    }
}

