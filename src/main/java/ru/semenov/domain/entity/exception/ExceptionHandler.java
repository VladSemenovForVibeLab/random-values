package ru.semenov.domain.entity.exception;

import org.springframework.http.ResponseEntity;

public interface ExceptionHandler {
    ResponseEntity<Object> handleException(Exception ex);
}
