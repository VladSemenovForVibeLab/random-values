package ru.semenov.domain.entity.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractErrorHandler implements ExceptionHandler {
    protected ResponseEntity<Object> createExceptionResponse(HttpStatus httpStatus, String message, LocalDateTime localDateTime){
        Map<String,Object> exceptionResponse = new HashMap<String,Object>();
        exceptionResponse.put("статус",httpStatus.value());
        exceptionResponse.put("ошибка",httpStatus.getReasonPhrase());
        exceptionResponse.put("сообщение",message);
        exceptionResponse.put("время",LocalDateTime.now());
        return new ResponseEntity<Object>(exceptionResponse,httpStatus);
    }
    protected ResponseEntity<Object> handleGenericException(Exception ex){
        return createExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR,ex.getMessage(),LocalDateTime.now());
    }
}
