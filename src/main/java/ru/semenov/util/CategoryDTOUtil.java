package ru.semenov.util;

import org.springframework.stereotype.Component;
import ru.semenov.domain.entity.CategoryEntity;
import ru.semenov.rest.model.in.CreateCategoryRestIn;
import ru.semenov.rest.model.in.UpdateCategoryRestIn;
import ru.semenov.rest.model.out.CategoryRestOut;
import ru.semenov.rest.model.out.CategoryOut;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryDTOUtil implements EntityDTOUtil<CategoryOut, CategoryRestOut, CategoryEntity, CreateCategoryRestIn, UpdateCategoryRestIn> {
    @Override
    public CategoryOut convertServiceToControllerForCreate(CreateCategoryRestIn entity) {
        return (CategoryOut) CategoryOut
                .builder()
                .name(entity.getName())
                .description(entity.getDescription())
                .build();
    }

    @Override
    public  CategoryRestOut convertServiceToController(CategoryOut element) {
        return (CategoryRestOut) CategoryRestOut
                .builder()
                .id(element.getId())
                .name(element.getName())
                .description(element.getDescription())
                .build();
    }

    @Override
    public CategoryOut convertEntityToService(CategoryEntity element) {
        return (CategoryOut) CategoryOut
                .builder()
                .id(element.getCategoryId())
                .name(element.getName())
                .description(element.getDescription())
                .build();
    }

    @Override
    public CategoryOut convertEntityToServiceForRequest(CategoryEntity element) {
        return null;
    }

    @Override
    public List<CategoryOut> convertEntityToServiceList(List<CategoryEntity> elements) {
        return elements.stream().map(this::convertEntityToService).collect(Collectors.toList());
    }

    @Override
    public CategoryOut toEntity(CategoryRestOut dto) {
        return null;
    }

    @Override
    public CategoryEntity toEntityForRequest(CreateCategoryRestIn dto) {
        return CategoryEntity
                .builder()
                .name(dto.getName())
                .description(dto.getDescription())
                .build();
    }


    @Override
    public List<CategoryRestOut> convertServiceToControllerList(List<CategoryOut> elements) {
        return elements.stream().map(this::convertServiceToController).toList();
    }

    @Override
    public List<CategoryOut> toEntity(List<CategoryRestOut> dtos) {
        return null;
    }

    @Override
    public CategoryRestOut convertCategoryRestOut(CategoryEntity save) {
        return CategoryRestOut
                .builder()
                .name(save.getName())
                .description(save.getDescription())
                .build();
    }

    @Override
    public CategoryEntity convertUpdateRestInToEntity(UpdateCategoryRestIn updateCategoryRestIn) {
        return CategoryEntity
                .builder()
                .name(updateCategoryRestIn.getName())
                .description(updateCategoryRestIn.getDescription())
                .build();
    }
}
