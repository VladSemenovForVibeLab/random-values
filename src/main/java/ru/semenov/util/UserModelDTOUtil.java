package ru.semenov.util;

import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import ru.semenov.domain.entity.UserEntity;
import ru.semenov.rest.model.in.UserModelDTORequestForRegister;

import java.util.List;

@Component
public class UserModelDTOUtil implements EntityDTOUtilUser<UserEntity, UserModelDTORequestForRegister> {

    @Override
    public UserModelDTORequestForRegister toDTO(UserEntity element) {
        return new UserModelDTORequestForRegister(element);
    }

    @Override
    public UserEntity toEntity(UserModelDTORequestForRegister dto) {
        return UserEntity
                .builder()
                .email(dto.getEmail())
                .password(dto.getPassword())
                .accountExpired(true)
                .active(true)
                .block(true)
                .name(dto.getName())
                .phoneNumber(dto.getPhoneNumber())
                .build();
    }

    @Override
    public List<UserModelDTORequestForRegister> toDTO(List<UserEntity> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<UserEntity> toEntity(List<UserModelDTORequestForRegister> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }

}