package ru.semenov.util;

import java.util.List;

public interface EntityDTOUtilUser<E,D> {
    public D toDTO(E element);
    public E toEntity(D dto);
    public List<D> toDTO(List<E> elements);
    public List<E> toEntity(List<D> dtos);

}

