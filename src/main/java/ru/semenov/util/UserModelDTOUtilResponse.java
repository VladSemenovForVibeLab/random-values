package ru.semenov.util;

import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import ru.semenov.domain.entity.UserEntity;
import ru.semenov.rest.model.out.UserModelResponse;

import java.util.List;

@Component
public class UserModelDTOUtilResponse implements EntityDTOUtilUser<UserEntity, UserModelResponse>{
    @Override
    public UserModelResponse toDTO(UserEntity element) {
        return new UserModelResponse(element);
    }

    @Override
    public UserEntity toEntity(UserModelResponse dto) {
        return UserEntity
                .builder()
                .phoneNumber(dto.getPhoneNumber())
                .email(dto.getEmail())
                .name(dto.getName())
                .build();
    }

    @Override
    public List<UserModelResponse> toDTO(List<UserEntity> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<UserEntity> toEntity(List<UserModelResponse> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }

}
