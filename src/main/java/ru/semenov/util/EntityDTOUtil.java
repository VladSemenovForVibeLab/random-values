package ru.semenov.util;

import ru.semenov.rest.model.in.UpdateCategoryRestIn;

import java.util.List;

public interface EntityDTOUtil<E,D,T,R,S> {
    E convertServiceToControllerForCreate(R entity);
    public D convertServiceToController(E element);
    public E convertEntityToService(T element);
    public E convertEntityToServiceForRequest(T element);
    public List<E> convertEntityToServiceList(List<T> elements);
    public E toEntity(D dto);
    public T toEntityForRequest(R dto);
    public List<D> convertServiceToControllerList(List<E> elements);
    public List<E> toEntity(List<D> dtos);

    D convertCategoryRestOut(T save);

    T convertUpdateRestInToEntity(S updateCategoryRestIn);
}

